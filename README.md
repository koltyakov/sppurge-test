# SPPurge test

## Dependencies

```bash
npm i
```

## Test preparation

Copy `./trash.txt` to `~site/SiteAssets`.

## Run

```bash
npm run purge
```