//@ts-check

const { AuthConfig } = require('node-sp-auth-config');
const { default: sppurge } = require('sppurge');

const authConfig = new AuthConfig({
  configPath: './config/private.json'
});

authConfig.getContext()
  .then(({ siteUrl, authOptions: creds }) => {
    const ctx = { siteUrl, creds };
    const filePath = `/${siteUrl.split('/').slice(3).join('/')}/SiteAssets/trash.txt`;
    return sppurge(ctx, { filePath });
  })
  .then(_ => console.log('Done'))
  .catch(err => console.log(err.message));